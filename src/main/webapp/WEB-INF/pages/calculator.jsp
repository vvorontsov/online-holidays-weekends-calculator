<%@taglib prefix="sec"
          uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/style.css">
</head>
<body>
<h1 class="title">Holidays + weekends calculator</h1>

<sec:authorize access="hasRole('ROLE_USER')">
    <c:if test="${not empty error}">
        <div class="error">${error}</div>
    </c:if>
    <form action="${pageContext.request.contextPath}/calculator" method="get">
        <label class="simple">Enter start date:
            <input type="date" name="start"
            <c:if test="${not empty startdate}">value="${startdate}"</c:if>">
        </label>
        <label class="simple">Enter finish date:
            <input type="date" name="finish" <c:if test="${not empty
            finishdate}">value="${finishdate}"</c:if>">
        </label>
        <br>
        <input type="submit" value="Calculate!">
    </form>
    <br>

    <c:if test="${not empty result}">
        <div class="msg">Total count of not working days is: ${result}</div>
    </c:if>

    <jsp:include page="logout_footer.jsp"/>


</sec:authorize>
</body>
</html>