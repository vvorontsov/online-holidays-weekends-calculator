SET FOREIGN_KEY_CHECKS=0;
DELETE FROM `user`;
ALTER TABLE `user` AUTO_INCREMENT=1;
DELETE FROM `holiday`;
ALTER TABLE `holiday` AUTO_INCREMENT=1;
SET FOREIGN_KEY_CHECKS=1;