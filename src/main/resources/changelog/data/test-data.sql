use weekend_counter;
insert into user (user_name,user_password) values ('first_user','first_user_password');
insert into user (user_name,user_password) values ('second_user','second_user_password');
insert into user (user_name,user_password) values ('third_user','third_user_password');
insert into holiday (holiday_day_of_month, holiday_month) values (1,1);
insert into holiday (holiday_day_of_month, holiday_month) values (7,1);
insert into holiday (holiday_day_of_month, holiday_month) values (8,3);
insert into holiday (holiday_day_of_month, holiday_month) values (8,4);
insert into holiday (holiday_day_of_month, holiday_month) values (1,5);
insert into holiday (holiday_day_of_month, holiday_month) values (27,5);
insert into holiday (holiday_day_of_month, holiday_month) values (25,12);