package com.pfsoft.testtask.core;

import com.pfsoft.testtask.core.time.TimeManager;
import com.pfsoft.testtask.entity.Holiday;

import java.time.LocalDate;
import java.util.List;

/**
 * This is library which provides methods for calculating count of weekend
 * and holidays.
 */
public class WeekendHolidayCounter {
    private static final LocalDate MONDAY_DAY = LocalDate.of(1900, 1, 1);
    private static final Integer COUNT_OF_DAYS_IN_WEEK = 7;
    private static final int MONDAY = 1;
    private static final int SATURDAY = 6;
    private static final int SUNDAY = 7;

    private WeekendHolidayCounter() {
    }

    /**
     * @param startDate start date
     * @param finishDate   finish date
     * @return count of weekends between start and finish dates. Both dates
     * are included.
     */
    public static Integer getCountOfWeekends(LocalDate startDate, LocalDate
            finishDate) {
        Integer countOfDays = getCountOfDays(startDate, finishDate);
        Integer dayOfWeekForStartDate = getDayOfWeek(startDate);

        Integer countOfDaysToStartOfNewWeek = COUNT_OF_DAYS_IN_WEEK + 1 -
                dayOfWeekForStartDate;
        Integer endPoint = getEndpoint(dayOfWeekForStartDate, countOfDays,
                countOfDaysToStartOfNewWeek);
        Integer countOfWeekends = 0;

        for (int i = dayOfWeekForStartDate; i <= endPoint; i++) {
            if ((i == SATURDAY) || (i == SUNDAY)) {
                countOfWeekends++;
            }
        }

        countOfDays -= countOfDaysToStartOfNewWeek;
        if (countOfDays <= 0) {
            return countOfWeekends;
        } else {
            countOfWeekends += 2 * (countOfDays / COUNT_OF_DAYS_IN_WEEK);
        }

        if ((countOfDays % COUNT_OF_DAYS_IN_WEEK) == SATURDAY) {
            countOfWeekends++;
        }
        return countOfWeekends;
    }

    /**
     * @param holidayList list of holidays. Holiday date is day and month
     *                    without year
     * @param start start date
     * @param finish finish date
     * @return count of holidays between start and finish date. Both dates
     * are included. If holiday is weekend - it will be not added to total
     * count of holidays.
     */
    public static Integer getCountOfHolidays(List<Holiday> holidayList,
                                             LocalDate start,
                                             LocalDate finish) {
        int compare = start.compareTo(finish);
        if (compare > 0) {
            throw new IllegalArgumentException();
        } else {
            return calculateHolidays(holidayList, start, finish);
        }
    }

    /**
     * @param startDate start date
     * @param finishDate finish date
     * @return total count of days between start and finish dates. Both dates
     * are included.
     */
    public static Integer getCountOfDays(LocalDate startDate, LocalDate
            finishDate) {
        int compare = startDate.compareTo(finishDate);
        if (compare > 0) {
            throw new IllegalArgumentException();
        } else {
            return calculateCountOfDays(startDate, finishDate);
        }
    }

    /**
     * @param date some date
     * @return index of day of a week
     * 1 - monday
     * 2 - tuesday
     * 3 - wednesday
     * 4 - thursday
     * 5 - friday
     * 6 - saturday
     * 7 - sunday
     */
    public static Integer getDayOfWeek(LocalDate date) {
        int compare = date.compareTo(MONDAY_DAY);
        if (compare > 0) {
            return getDayOfWeekAfter01Jan1900(date);
        } else if (compare < 0) {
            return getDayOfWeekBefore01Jan1900(date);
        } else return MONDAY;
    }

    /**
     * @param date some date
     * @return true if input date is weekend, otherwise false.
     */
    public static boolean isWeekend(LocalDate date) {
        Integer dayOfWeek = getDayOfWeek(date);
        if ((dayOfWeek == SATURDAY) || (dayOfWeek == SUNDAY)) {
            return true;
        } else {
            return false;
        }
    }

    private static Integer calculateHolidays(List<Holiday> holidayList,
                                             LocalDate start,
                                             LocalDate finish) {
        Integer count = 0;
        for (int year = start.getYear(); year <= finish.getYear(); year++)
            for (Holiday holiday : holidayList) {
                LocalDate holidayDate = makeDate(holiday, year);
                if ((holidayDate.compareTo(start) >= 0) &&
                        (holidayDate.compareTo(finish) <= 0) &&
                        !isWeekend(holidayDate)) {
                    count++;
                }
            }
        return count;
    }

    private static LocalDate makeDate(Holiday input, int year) {
        int month = input.getHolidayMonth();
        int day = input.getHolidayDayOfMonth();
        return LocalDate.of(year, month, day);
    }

    private static Integer getEndpoint(Integer start, Integer count, Integer
            toNewWeek) {
        if (toNewWeek > count) {
            return (start + count - 1);
        } else {
            return COUNT_OF_DAYS_IN_WEEK;
        }
    }

    private static Integer calculateCountOfDays(LocalDate startDate,
                                                LocalDate finishDate) {
        Integer afterStart = TimeManager.getCountOfDaysFromDateToEndOfYear
                (startDate);
        Integer beforeEnd = TimeManager.getCountOfDaysFromStartOfYearToDate
                (finishDate);
        Integer startYear = startDate.getYear();
        Integer endYear = finishDate.getYear();
        Integer res = afterStart + beforeEnd;
        if (!startYear.equals(endYear)) {
            for (int year = (startYear + 1); year < endYear; year++) {
                res += TimeManager.getCountOfDaysInYear(year);
            }
        } else {
            res -= TimeManager.getCountOfDaysInYear(startYear);
        }
        return res;
    }

    private static Integer getDayOfWeekAfter01Jan1900(LocalDate date) {
        Integer countOfDays = getCountOfDays(MONDAY_DAY, date);
        Integer dayOfWeek = countOfDays % COUNT_OF_DAYS_IN_WEEK;
        if (dayOfWeek.equals(0)) {
            dayOfWeek = 7;
        }
        return dayOfWeek;
    }

    private static Integer getDayOfWeekBefore01Jan1900(LocalDate date) {
        Integer countOfDays = getCountOfDays(date, MONDAY_DAY) - 2;
        Integer dayOfWeek = countOfDays % COUNT_OF_DAYS_IN_WEEK;
        dayOfWeek = COUNT_OF_DAYS_IN_WEEK - dayOfWeek;
        return dayOfWeek;
    }
}