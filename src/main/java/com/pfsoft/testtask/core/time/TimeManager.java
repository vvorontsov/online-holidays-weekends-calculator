package com.pfsoft.testtask.core.time;

import com.pfsoft.testtask.core.time.entity.Months;

import java.time.LocalDate;

/**
 * Class provides methods to count days for some period.
 */
public class TimeManager {
    private static final Integer MULTIPLICITY_OF_CENTURY_START_YEAR = 100;
    private static final Integer MULTIPLICITY_OF_LEAP_YEAR = 4;
    private static final Integer MULTIPLICITY_OF_LEAP_YEAR_AT_START_OF_CENTURY = 400;
    private static final Integer COUNT_OF_MONTH_IN_YEAR = 12;
    private static final Integer COUNT_OF_DAYS_IN_YEAR;
    private static final Integer COUNT_OF_DAYS_IN_LEAP_YEAR;

    static {
        Integer res = 0;
        for (int i = 1; i <= COUNT_OF_MONTH_IN_YEAR; i++) {
            for (Months month : Months.values()) {
                if (i == month.getIndex()) {
                    res += month.getCountOfDays();
                }
            }
        }
        COUNT_OF_DAYS_IN_YEAR = res;
        COUNT_OF_DAYS_IN_LEAP_YEAR = res + 1;
    }

    private TimeManager() {
    }

    /**
     * @param date some date
     * @return count of days from start of the year (which is specified at
     * date). Date is included in result count.
     * <p>
     * For example for date: 2016-03-15 it will return result =
     * 31 + 29 + 15 = 75.
     */
    public static Integer getCountOfDaysFromStartOfYearToDate(LocalDate date) {
        Integer res = 0;
        for (int i = 1; i < date.getMonthValue(); i++) {
            for (Months month : Months.values()) {
                if (i == month.getIndex()) {
                    res += getCountOfDaysInMonth(month, date.getYear());
                }
            }
        }
        res += date.getDayOfMonth();
        return res;
    }

    /**
     * @param date some date
     * @return count of days from specified date to end of the year (which
     * specified in date). Date is included in result count.
     * <p>
     * For example for date: 28-11-2017 it will return next:
     * (30 - 27) + 31 = 34.
     */
    public static Integer getCountOfDaysFromDateToEndOfYear(LocalDate date) {
        Integer res = getCountOfDaysFromStartOfYearToDate(date);
        res = getCountOfDaysInYear(date.getYear()) - res + 1;
        return res;
    }

    /**
     * @param year number of year
     * @return count of days in specified year.
     */
    public static Integer getCountOfDaysInYear(Integer year) {
        if (isLeap(year)) {
            return COUNT_OF_DAYS_IN_LEAP_YEAR;
        } else {
            return COUNT_OF_DAYS_IN_YEAR;
        }
    }


    private static Integer getCountOfDaysInMonth(Months month, Integer year) {
        Integer res = month.getCountOfDays();
        if ((month.getIndex() == 2) && (isLeap(year))) {
            res++;
        }
        return res;
    }

    /**
     * @param year number of year
     * @return true if year is leap, false in other case.
     */
    private static boolean isLeap(Integer year) {
        if ((year % MULTIPLICITY_OF_LEAP_YEAR) == 0) {
            return isLeapForPotentialLeapYear(year);
        } else {
            return false;
        }
    }

    private static boolean isLeapForPotentialLeapYear(Integer year) {
        if ((year % MULTIPLICITY_OF_CENTURY_START_YEAR) == 0) {
            return isLeapForYearOfCenturyStart(year);
        } else {
            return true;
        }
    }

    private static boolean isLeapForYearOfCenturyStart(Integer year) {
        if ((year % MULTIPLICITY_OF_LEAP_YEAR_AT_START_OF_CENTURY) == 0) {
            return true;
        } else {
            return false;
        }
    }
}
