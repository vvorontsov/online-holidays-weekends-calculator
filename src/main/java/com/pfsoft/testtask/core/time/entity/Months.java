package com.pfsoft.testtask.core.time.entity;

/**
 * Enum which contains months with their duration (days) and order (specified
 * by id) for not leap year.
 */
public enum Months {
    JANUARY(1, 31),
    FEBRUARY(2, 28),
    MARCH(3, 31),
    APRIL(4, 30),
    MAY(5, 31),
    JUNE(6, 30),
    JULY(7, 31),
    AUGUST(8, 31),
    SEPTEMBER(9, 30),
    OCTOBER(10, 31),
    NOVEMBER(11, 30),
    DECEMBER(12, 31);

    private final int countOfDays;
    private final int index;

    Months(int index, int count) {
        this.index = index;
        this.countOfDays = count;
    }

    public int getCountOfDays() {
        return countOfDays;
    }

    public int getIndex() {
        return index;
    }
}
