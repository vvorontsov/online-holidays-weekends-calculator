package com.pfsoft.testtask.service.abstraction;

import java.time.LocalDate;

/**
 * Implementation should contain method that parse date from some string
 * pattern.
 * Furthermore, it should calculate total count of weekends and holidays
 * between some dates (they are included). If holiday day is weekend - it
 * pluses only one day to total count.
 */
public interface HolidaysWeekendsCalculator {
    LocalDate parseDate(String date);

    Integer calculateHolidaysAndWeekends(String date1, String date2);
}
