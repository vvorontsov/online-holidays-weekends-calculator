package com.pfsoft.testtask.service.abstraction;

import com.pfsoft.testtask.entity.User;

/**
 * Implementation should contain CRUD operations + it should be able to find
 * user by username (through username is unique value).
 */
public interface UserServiceFunctionality extends ServiceCRUD<User>{
    User findUser(String username);
}
