package com.pfsoft.testtask.service;

import com.pfsoft.testtask.core.WeekendHolidayCounter;
import com.pfsoft.testtask.entity.Holiday;
import com.pfsoft.testtask.service.abstraction.HolidaysWeekendsCalculator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class CalculationService implements HolidaysWeekendsCalculator {

    @Autowired
    private HolidayService holidayService;

    /**
     * Format of date: YYYY-MM-DD. Example of matching string: 2018-12-13.
     */
    private String datePattern = "^(\\d{4})-([0-1]\\d)-([0-3]\\d)$";

    /**
     * @param date input string with date
     * @return instance of LocalDate object parsed from input string. If
     * input string doesn't match pattern - returns null.
     */
    @Override
    public LocalDate parseDate(String date) {
        LocalDate retDate = null;
        Pattern pattern = Pattern.compile(datePattern);
        Matcher matcher = pattern.matcher(date);
        if (matcher.find()) {
            int year = Integer.parseInt(matcher.group(1));
            int month = Integer.parseInt(matcher.group(2));
            int day = Integer.parseInt(matcher.group(3));
            retDate = LocalDate.of(year, month, day);
        }
        return retDate;
    }

    /**
     * @param date1 start date
     * @param date2 finish date
     * @return count of weekends and holidays between start and finish dates.
     * Both dates are included. If holiday is weekend it pluses onny one day
     * to total count.
     * @throws NullPointerException if some of input strings doesn't match
     * pattern.
     * @throws IllegalArgumentException If finish date is earlier then start
     * date.
     */
    @Override
    public Integer calculateHolidaysAndWeekends(String date1, String date2) throws
            NullPointerException, IllegalArgumentException {
        LocalDate start = parseDate(date1);
        LocalDate finish = parseDate(date2);
        if ((start == null) && (finish == null)) {
            throw new NullPointerException();
        }
        Integer count = WeekendHolidayCounter.getCountOfWeekends(start, finish);
        List<Holiday> holidays = holidayService.selectAll();
        count += WeekendHolidayCounter.getCountOfHolidays(holidays, start, finish);
        return count;
    }
}
