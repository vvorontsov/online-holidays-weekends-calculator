package com.pfsoft.testtask.service;

/**
 * Class provides singletons of each service.
 */
public class SingletonService {
    private static UserService userService;
    private static HolidayService holidayService;

    private SingletonService() {
    }

    public static UserService getUserService() {
        if (userService == null) {
            userService = new UserService();
        }
        return userService;
    }

    public static HolidayService getHolidayService() {
        if (holidayService == null) {
            holidayService = new HolidayService();
        }
        return holidayService;
    }
}
