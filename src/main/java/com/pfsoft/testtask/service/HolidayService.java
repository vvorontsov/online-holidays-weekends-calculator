package com.pfsoft.testtask.service;

import com.pfsoft.testtask.dao.element.functionality.HolidayFunctionality;
import com.pfsoft.testtask.entity.Holiday;
import com.pfsoft.testtask.service.abstraction.ServiceCRUD;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HolidayService implements ServiceCRUD<Holiday> {

    @Autowired
    private HolidayFunctionality holidayDAO;

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void insert(Holiday entity) {
        holidayDAO.addElement(entity, sessionFactory);
    }

    @Override
    public List<Holiday> selectAll() {
        return holidayDAO.getAllElements(sessionFactory);
    }

    @Override
    public Holiday selectById(Integer id) {
        return holidayDAO.getElementByID(id, sessionFactory);
    }

    @Override
    public void update(Holiday entity) {
        holidayDAO.updateElement(entity, sessionFactory);
    }

    @Override
    public void delete(Holiday entity) {
        holidayDAO.deleteElement(entity, sessionFactory);
    }
}
