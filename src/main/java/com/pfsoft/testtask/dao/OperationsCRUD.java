package com.pfsoft.testtask.dao;

import org.hibernate.SessionFactory;

import java.util.List;

/**
 * DAO interface. Includes CRUD operations. Uses provided session.
 */
public interface OperationsCRUD<E> {

    void addElement(E element, SessionFactory sessionFactory);

    void updateElement(E element, SessionFactory sessionFactory);

    E getElementByID(Integer elementId, SessionFactory sessionFactory);

    List<E> getAllElements(SessionFactory sessionFactory);

    void deleteElement(E element, SessionFactory sessionFactory);
}
