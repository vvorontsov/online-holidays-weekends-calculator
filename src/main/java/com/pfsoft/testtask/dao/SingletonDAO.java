package com.pfsoft.testtask.dao;

import com.pfsoft.testtask.dao.element.HolidayDAO;
import com.pfsoft.testtask.dao.element.UserDAO;

/**
 * Class provides singletons of each DAO.
 */
public class SingletonDAO {

    private static UserDAO userDAO;
    private static HolidayDAO holidayDAO;

    private SingletonDAO() {}

    public static UserDAO getUserDAO() {
        if (userDAO == null) {
            userDAO = new UserDAO();
        }
        return userDAO;
    }

    public static HolidayDAO getHolidayDAO() {
        if (holidayDAO == null) {
            holidayDAO = new HolidayDAO();
        }
        return holidayDAO;
    }
}
