package com.pfsoft.testtask.dao.element.functionality;

import com.pfsoft.testtask.dao.OperationsCRUD;
import com.pfsoft.testtask.entity.User;
import org.hibernate.SessionFactory;

/**
 * Implementation should contain CRUD operations + it should be able to find
 * user by username (through username is unique value). Session is provided.
 */
public interface UserFunctionality extends OperationsCRUD<User> {
    User findUser(String username, SessionFactory sessionFactory);
}
