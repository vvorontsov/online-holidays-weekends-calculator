package com.pfsoft.testtask.dao.element.functionality;

import com.pfsoft.testtask.dao.OperationsCRUD;
import com.pfsoft.testtask.entity.Holiday;

/**
 * Implementation should contain CRUD operations. Session is provided.
 */
public interface HolidayFunctionality extends OperationsCRUD<Holiday> {
}
