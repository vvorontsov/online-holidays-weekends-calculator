package com.pfsoft.testtask.dao.element;

import com.pfsoft.testtask.dao.ElementDAO;
import com.pfsoft.testtask.dao.element.functionality.HolidayFunctionality;
import com.pfsoft.testtask.entity.Holiday;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class HolidayDAO extends ElementDAO<Holiday> implements HolidayFunctionality {

    public HolidayDAO() {
        super(Holiday.class);
    }
}
