package com.pfsoft.testtask.controller;

import com.pfsoft.testtask.service.abstraction.HolidaysWeekendsCalculator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 * Main controller between model and view.
 */
@Controller
@PropertySource("classpath:message.properties")
public class MainController {

    private final Environment env;

    @Autowired
    private HolidaysWeekendsCalculator calculationService;

    @Autowired
    public MainController(Environment env) {
        this.env = env;
    }

    /**
     * Method directs visitor of web site to login page.
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView home() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("login_page");
        return modelAndView;
    }

    /**
     * @param error in the case of login error.
     * @param logout in the case of successful logout.
     * @return login page.
     */
    @RequestMapping(value = "/login_page", method = RequestMethod.GET)
    public ModelAndView login(@RequestParam(value = "error", required = false) String error,
                              @RequestParam(value = "logout", required = false) String logout) {
        ModelAndView model = new ModelAndView();
        if (error != null) {
            model.addObject("error", env.getProperty("mes.err.login"));
        }
        if (logout != null) {
            model.addObject("msg", env.getProperty("mes.logout"));
        }
        model.setViewName("login_page");
        return model;
    }

    /**
     * User will see this page if he will try to access some page which he
     * isn't able to access through his rights.
     * It isn't used right now. Can be useful at future.
     * @return error message.
     */
    @RequestMapping(value = "/403", method = RequestMethod.GET)
    public ModelAndView accessDenied() {
        ModelAndView model = new ModelAndView();
        //check if user is login
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (!(auth instanceof AnonymousAuthenticationToken)) {
            UserDetails userDetail = (UserDetails) auth.getPrincipal();
            model.addObject("username", userDetail.getUsername());
        }
        model.setViewName("403");
        return model;
    }

    /**
     * @param start start date from previous page
     * @param finish finish date from previous page
     * @return calculator page. If start and finish dates are correct -
     * prints message with result, otherwise - error message.
     */
    @RequestMapping(value = "/calculator", method = RequestMethod.GET)
    public ModelAndView calculatorPage(
            @RequestParam(value = "start", required = false) String start,
            @RequestParam(value = "finish", required = false) String finish) {
        ModelAndView model = new ModelAndView();
        Integer count;
        String err = null;
        try {
            count = calculationService.calculateHolidaysAndWeekends(start, finish);
            model.addObject("result", count);
        } catch (NullPointerException ex) {
            err = env.getProperty("mes.err.empty.input");
        } catch (IllegalArgumentException ex) {
            err = env.getProperty("mes.err.queue");
        }
        model.addObject("startdate",start);
        model.addObject("finishdate",finish);
        model.addObject("error", err);
        model.setViewName("calculator");
        return model;
    }
}