package com.pfsoft.testtask.config;

import com.pfsoft.testtask.dao.SingletonDAO;
import com.pfsoft.testtask.dao.element.functionality.HolidayFunctionality;
import com.pfsoft.testtask.dao.element.functionality.UserFunctionality;
import com.pfsoft.testtask.service.SingletonService;
import com.pfsoft.testtask.service.HolidayService;
import com.pfsoft.testtask.service.UserService;
import liquibase.integration.spring.SpringLiquibase;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

/**
 * Spring bean store.
 */
@Configuration
@ComponentScan("com.pfsoft.testtask.*")
@EnableTransactionManagement
@PropertySource("classpath:user.properties")
public class ApplicationContextConfig {

    // The Environment class serves as the property holder
    // and stores all the properties loaded by the @PropertySource
    private final Environment env;

    @Autowired
    public ApplicationContextConfig(Environment env) {
        this.env = env;
    }

    @Bean(name = "dataSource")
    public DataSource getDataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();

        // See: user.properties
        dataSource.setDriverClassName(env.getProperty("ds.database-driver"));
        dataSource.setUrl(env.getProperty("ds.url"));
        dataSource.setUsername(env.getProperty("ds.username"));
        dataSource.setPassword(env.getProperty("ds.password"));

        return dataSource;
    }

    @Bean
    public SpringLiquibase liquibase() {
        SpringLiquibase liquibase = new SpringLiquibase();

        liquibase.setDataSource(getDataSource());
        liquibase.setChangeLog("classpath:changelog/liquibase-changeLog.xml");

        return liquibase;
    }

    @Autowired
    @Bean(name = "sessionFactory")
    public SessionFactory getSessionFactory(DataSource dataSource) throws Exception {
        Properties properties = new Properties();

        // See: user.properties
        properties.put("hibernate.dialect", env.getProperty("hibernate.dialect"));
        properties.put("hibernate.show_sql", env.getProperty("hibernate.show_sql"));
        properties.put("current_session_context_class", env.getProperty("current_session_context_class"));
        LocalSessionFactoryBean factoryBean = new LocalSessionFactoryBean();

        factoryBean.setPackagesToScan("com.pfsoft.testtask.entity");
        factoryBean.setDataSource(dataSource);
        factoryBean.setHibernateProperties(properties);
        factoryBean.afterPropertiesSet();

        SessionFactory sf = factoryBean.getObject();
        System.out.println("## getSessionFactory: " + sf);
        return sf;
    }

    @Autowired
    @Bean(name = "transactionManager")
    public HibernateTransactionManager getTransactionManager(SessionFactory sessionFactory) {
        HibernateTransactionManager transactionManager = new HibernateTransactionManager(sessionFactory);

        return transactionManager;
    }

    @Bean(name = "userDAO")
    public UserFunctionality getUserDAO() {
        return SingletonDAO.getUserDAO();
    }

    @Bean(name = "holidayDAO")
    public HolidayFunctionality getHolidayDAO() {
        return SingletonDAO.getHolidayDAO();
    }

    @Bean(name = "userService")
    public UserService getUserService() {
        return SingletonService.getUserService();
    }

    @Bean(name = "holidayService")
    public HolidayService getHolidayService() {
        return SingletonService.getHolidayService();
    }
}