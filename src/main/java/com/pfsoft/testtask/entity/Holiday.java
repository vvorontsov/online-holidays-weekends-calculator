package com.pfsoft.testtask.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entity class for table "holiday". Element of this class represents date
 * without year, e.g. day = 5, month = 3 is 5th of March.
 */
@Entity
@Table(name = "holiday")
public class Holiday {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "holiday_id")
    private Integer holidayId;

    @Column(name = "holiday_day_of_month")
    private Integer holidayDayOfMonth;

    @Column(name = "holiday_month")
    private Integer holidayMonth;

    public Integer getHolidayId() {
        return holidayId;
    }

    public void setHolidayId(Integer holidayId) {
        this.holidayId = holidayId;
    }

    public Integer getHolidayDayOfMonth() {
        return holidayDayOfMonth;
    }

    public void setHolidayDayOfMonth(Integer holidayDayOfMonth) {
        this.holidayDayOfMonth = holidayDayOfMonth;
    }

    public Integer getHolidayMonth() {
        return holidayMonth;
    }

    public void setHolidayMonth(Integer holidayMonth) {
        this.holidayMonth = holidayMonth;
    }
}
