package core;

import com.pfsoft.testtask.core.WeekendHolidayCounter;
import com.pfsoft.testtask.entity.Holiday;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class WeekendHolidayCounterTest {
    @Test
    public void getCountOfDays_differentYears() {
        LocalDate start = LocalDate.of(2015, 12, 30);
        LocalDate end = LocalDate.of(2018, 1, 2);

        Integer res = WeekendHolidayCounter.getCountOfDays(start, end);
        int expected = 2 + 365 + 366 + 2;

        Assert.assertEquals(res.intValue(), expected);
    }

    @Test
    public void getCountOfDays_sameDate() {
        LocalDate start = LocalDate.of(2015, 10, 23);
        LocalDate end = LocalDate.of(2015, 10, 23);

        Integer res = WeekendHolidayCounter.getCountOfDays(start, end);

        Assert.assertEquals(res.intValue(), 1);
    }

    @Test
    public void getDayOfWeekTest_Now() {
        LocalDate testDate = LocalDate.now();

        Integer res = WeekendHolidayCounter.getDayOfWeek(testDate);
        Integer expected = testDate.getDayOfWeek().getValue();

        Assert.assertEquals(res.intValue(), expected.intValue());
    }

    @Test
    public void getDayOfWeek_Before1900() {
        //LocalDate testDate = LocalDate.of(1879, 5, 1);
        LocalDate testDate = LocalDate.of(1879, 9, 1);

        Integer res = WeekendHolidayCounter.getDayOfWeek(testDate);

        Assert.assertEquals(res.intValue(), 1);
    }

    @Test
    public void getCountOfWeekends_moreThenOneMonthDifference() {
        LocalDate start = LocalDate.of(2018, 1, 28);
        LocalDate end = LocalDate.of(2018, 3, 3);

        Integer res = WeekendHolidayCounter.getCountOfWeekends(start, end);

        Assert.assertEquals(res.intValue(), 10);
    }

    @Test
    public void getCountOfWeekends_noWeekendsBetween() {
        LocalDate start = LocalDate.of(2018, 1, 1);
        LocalDate end = LocalDate.of(2018, 1, 5);

        Integer res = WeekendHolidayCounter.getCountOfWeekends(start, end);

        Assert.assertEquals(res.intValue(), 0);
    }

    @Test
    public void getCountOfWeekends_oneWeekendBetweenDates() {
        LocalDate start = LocalDate.of(2018, 1, 7);
        LocalDate end = LocalDate.of(2018, 1, 10);

        Integer res = WeekendHolidayCounter.getCountOfWeekends(start, end);

        Assert.assertEquals(res.intValue(), 1);
    }

    @Test
    public void getCountOfHolidays_fullYear() {
        List<Holiday> holidays = new ArrayList<>();
        holidays.add(getHoliday(1,1));
        holidays.add(getHoliday(7,1));
        holidays.add(getHoliday(8,3));
        holidays.add(getHoliday(8,4));
        holidays.add(getHoliday(1,5));
        holidays.add(getHoliday(27,5));
        holidays.add(getHoliday(25,12));
        LocalDate start = LocalDate.of(2018,1,1);
        LocalDate finish = LocalDate.of(2018,12,31);

        Integer countOfHolidays = WeekendHolidayCounter.getCountOfHolidays(holidays,
                start, finish);

        Assert.assertEquals(countOfHolidays.intValue(), 4);
    }

    @Test
    public void isWeekend_monday_false() {
        LocalDate monday = LocalDate.of(1900, 1, 1);

        Assert.assertTrue(!WeekendHolidayCounter.isWeekend(monday));
    }

    @Test
    public void isWeekend_sunday_true() {
        LocalDate sunday = LocalDate.of(1900, 1, 7);

        Assert.assertTrue(WeekendHolidayCounter.isWeekend(sunday));
    }


    private Holiday getHoliday(Integer day, Integer month) {
        Holiday holiday = new Holiday();
        holiday.setHolidayDayOfMonth(day);
        holiday.setHolidayMonth(month);
        return holiday;
    }
}

