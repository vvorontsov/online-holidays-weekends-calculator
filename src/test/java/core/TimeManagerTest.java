package core;

import com.pfsoft.testtask.core.time.TimeManager;
import com.pfsoft.testtask.core.time.entity.Months;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.time.LocalDate;

public class TimeManagerTest {

    private static final int YEAR = 365;
    private static final int LEAP_YEAR = 366;

    @Test
    public void getCountOfDaysInYear_notLeapYear_result365() {
        Integer res = TimeManager.getCountOfDaysInYear(2018);

        Assert.assertEquals(res.intValue(), YEAR);
    }

    @Test
    public void getCountOfDaysInYear_commonLeapYear_result366() {
        Integer res = TimeManager.getCountOfDaysInYear(2016);

        Assert.assertEquals(res.intValue(), LEAP_YEAR);
    }

    @Test
    public void getCountOfDaysInYear_notLeapYearCenturyStart_result365() {
        Integer res = TimeManager.getCountOfDaysInYear(1500);

        Assert.assertEquals(res.intValue(), YEAR);
    }

    @Test
    public void getCountOfDaysInYear_leapYearCenturyStart_result366() {
        Integer res = TimeManager.getCountOfDaysInYear(1600);

        Assert.assertEquals(res.intValue(), LEAP_YEAR);
    }

    @Test
    public void getCountOfDaysFromStartOfYearToDate_notLeapYear() {
        LocalDate localDate = LocalDate.of(2018, 3, 15);
        int expected = Months.JANUARY.getCountOfDays()
                + Months.FEBRUARY.getCountOfDays() + 15;

        Integer res = TimeManager.getCountOfDaysFromStartOfYearToDate
                (localDate);

        Assert.assertEquals(res.intValue(), expected);
    }

    @Test
    public void getCountOfDaysFromStartOfYearToDate_leapYear() {
        LocalDate localDate = LocalDate.of(2016, 3, 15);
        int expected = Months.JANUARY.getCountOfDays()
                + Months.FEBRUARY.getCountOfDays() + 1 + 15;

        Integer res = TimeManager.getCountOfDaysFromStartOfYearToDate
                (localDate);

        Assert.assertEquals(res.intValue(), expected);
    }

    @Test
    public void getCountOfDaysFromDateToEndOfYear_notLeapYear() {
        LocalDate localDate = LocalDate.of(2017, 10, 15);
        int expected = Months.OCTOBER.getCountOfDays()
                + Months.NOVEMBER.getCountOfDays()
                + Months.DECEMBER.getCountOfDays() - 15 + 1;

        Integer res = TimeManager.getCountOfDaysFromDateToEndOfYear(localDate);

        Assert.assertEquals(res.intValue(), expected);
    }

    @Test
    public void getCountOfDaysFromDateToEndOfYear_leapYear() {
        LocalDate localDate = LocalDate.of(2016, 10, 15);
        int expected = Months.OCTOBER.getCountOfDays()
                + Months.NOVEMBER.getCountOfDays()
                + Months.DECEMBER.getCountOfDays() - 15 + 1;

        Integer res = TimeManager.getCountOfDaysFromDateToEndOfYear(localDate);

        Assert.assertEquals(res.intValue(), expected);
    }
}
