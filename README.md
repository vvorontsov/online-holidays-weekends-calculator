# Online holidays + weekends calculator
---
##To run this web application do the next:
- clone this repository: https://vvorontsov@bitbucket.org/vvorontsov/online-holidays-weekends-calculator.git
- create ***mysql*** database named "weekend_counter", add user "admin" with password "supersecurepwd". Example of script:
```sql
create schema if not exists weekend_counter;

create user if not exists 'admin'@'localhost' IDENTIFIED BY 'supersecurepwd';
```
- do: ***mvn clean package***;
- go to ***liquibase*** plugin and run command **"update"**;
- go to target folder and deploy "holidays-calculator-1.1-RELEASE.war" file to **Tomcat**;
- go to browser and run it by URL: <http://localhost:8080/holidays-calculator-1.1-RELEASE> (default tomcat localhost is 8080).
+ credentials: 
  + username: **first_user**
  + password: **first_user_password**